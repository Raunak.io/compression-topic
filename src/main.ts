import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as compression from 'compression';
// import * as compression from 'fastify-compress';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // app.register(compression)// when use fastify
  // app.register(compression,{encodings:['gzip','deflate']})// to prevent brotli to compress
  app.use(compression()); // used globally to decrease the size of response data
  await app.listen(3000);
}
bootstrap();
